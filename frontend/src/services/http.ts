import axios from 'axios'
import router from '../router/index'

const instance = axios.create({
  baseURL: 'http://localhost:3000'
})

function delay(sec: number) {
  return new Promise((resolve, reject) => {
    setTimeout(() => resolve(sec), sec * 1000)
  })
}

instance.interceptors.request.use(async function (config) {
  const token = localStorage.getItem('accessToken')
  if (token) {
    config.headers.Authorization = `Bearer ${token}`
  }
  await delay(1)
  return config
})

instance.interceptors.response.use(
  async function (res) {
    await delay(1)
    return res
  },
  function (error) {
    if (error.response.status === 401) {
      console.log('Hey 401')
      router.replace('/login')
    }
    return Promise.reject(error)
  }
)
export default instance
