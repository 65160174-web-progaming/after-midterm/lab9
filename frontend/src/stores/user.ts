import { useLoadingStore } from './loading'
import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import userService from '@/services/user'
import type { User } from '@/types/User'
import { useMessageStore } from './message'

export const useUserStore = defineStore('user', () => {
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const users = ref<User[]>([])
  const initialUser: User & {files: File[]} = {
    email: 'UserTest@email.com',
    password: 'Pass@1234',
    fullName: 'UserTest',
    gender: 'male',
    roles: [{ id: 2, name: 'user' }],
    image: 'noimage.jpg',
    files: []
  }
  const editedUser = ref<User & { files: File[] }>(JSON.parse(JSON.stringify(initialUser)))

  async function getUser(id: number) {
    loadingStore.doLoad()
    const res = await userService.getUser(id)
    editedUser.value = res.data
    loadingStore.finish()
  }
  async function getUsers() {
    try {
      loadingStore.doLoad()
      const res = await userService.getUsers()
      users.value = res.data
      loadingStore.finish()
    } catch (error) {
      loadingStore.finish()
    }
  }
  async function saveUser() {
    try {
      loadingStore.doLoad()
      const user = editedUser.value
      if (!user.id) {
        // Add new
        console.log('Post ' + JSON.stringify(user))
        const res = await userService.addUser(user)
      } else {
        // Update
        console.log('Patch ' + JSON.stringify(user))
        const res = await userService.updateUser(user)
      }
  
      await getUsers()
      loadingStore.finish()
    } catch (error:any) {
      loadingStore.finish()
      messageStore.showMessage(error.message)
    }
  }
  async function deleteUser() {
    loadingStore.doLoad()
    const user = editedUser.value
    const res = await userService.delUser(user)

    await getUsers()
    loadingStore.finish()
  }

  function clearForm() {
    editedUser.value = JSON.parse(JSON.stringify(initialUser))
  }
  return { users, getUsers, saveUser, deleteUser, editedUser, getUser, clearForm }
})
